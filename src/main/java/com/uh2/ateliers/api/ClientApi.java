package com.uh2.ateliers.api;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.dto.ClientDto;
import com.uh2.ateliers.service.ClientService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/client")
public class ClientApi {

	@Autowired
	private ClientService clientService;
	
	@GetMapping("clients")
	public ResponseEntity<List<Client>> getAllClients() {
		return ResponseEntity.ok().body(clientService.findAll());
	}
	
	@GetMapping("client/{id}")
	public ResponseEntity<Client> getClient(@PathVariable(value="id") Long clientId) {
		return ResponseEntity.ok().body(clientService.findById(clientId).get());
	}
	
	@PostMapping("ajout")
	public Client ajoutClient(@Valid @RequestBody ClientDto clientDto) {
		return clientService.save(clientDto);
	}
	
	@PutMapping("modif/{id}")
	public ResponseEntity<Client> modifClient(@PathVariable(value="id") Long clientId, @Valid @RequestBody ClientDto clientDto) {
		
		Optional<Client> client = clientService.findById(clientDto.getId());
		
		if(!client.isPresent()) 
			return new ResponseEntity<Client>(HttpStatus.BAD_REQUEST);
		
//		Client client_update = client.get();
//		client_update.setNomClient(clientDto.getNomClient());
//		client_update.setTelephone(clientDto.getTelephone());
//		client_update.setAdresse(clientDto.getAdresse());
//		client_update.setEmail(clientDto.getEmail());
//		client_update.setFactures(clientDto.getFactures());
		
		clientService.save(clientDto);
		
		return new ResponseEntity<Client>(HttpStatus.OK);
		
	}
	
	
	@DeleteMapping("supprime/{id}")
	public ResponseEntity<Client> supprimeClient(@PathVariable(value="id") Long clientId, @Valid @RequestBody ClientDto clientDto) {
		
		Optional<Client> client = clientService.findById(clientId);
		
		if(!client.isPresent()) 
			return new ResponseEntity<Client>(HttpStatus.BAD_REQUEST);
		
//		Client client_update = client.get();
//		client_update.setNomClient(clientDto.getNomClient());
//		client_update.setTelephone(clientDto.getTelephone());
//		client_update.setAdresse(clientDto.getAdresse());
//		client_update.setEmail(clientDto.getEmail());
//		client_update.setFactures(clientDto.getFactures());
		
		clientService.remove(clientDto);
		
		return new ResponseEntity<Client>(HttpStatus.OK);
		
	}
	
	
	
	
	
}
