package com.uh2.ateliers.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.UserDto;

public interface UserService extends UserDetailsService {
	
	User findByUsername(String username);
	
	User save(UserDto registration);

}
