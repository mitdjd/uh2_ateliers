package com.uh2.ateliers.service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.access.prepost.PreAuthorize;

import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.dto.ClientDto;

public interface ClientService {

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	Client save(ClientDto clientDto);
	
	Client update(ClientDto clientDto);
	
	List<Client> findAll();
	
	void remove(ClientDto clientDto);
	
	Client findByNomClient(String nomClient);
	
	Optional<Client> findById(long id);
	
}
