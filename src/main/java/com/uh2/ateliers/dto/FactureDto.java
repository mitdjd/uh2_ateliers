package com.uh2.ateliers.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.uh2.ateliers.domain.Client;

public class FactureDto {
	
	private Long id;
	
	private Client client;
	
	@NotNull
	private String reference;
	
	@NotNull
	private Date dateEcheance;
	
	private double montantTotal;

	public FactureDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FactureDto(Long id, Client client, String reference, Date dateEcheance, double montantTotal) {
		super();
		this.id = id;
		this.client = client;
		this.reference = reference;
		this.dateEcheance = dateEcheance;
		this.montantTotal = montantTotal;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDateEcheance() {
		return dateEcheance;
	}

	public void setDateEcheance(Date dateEcheance) {
		this.dateEcheance = dateEcheance;
	}

	public double getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	

}
