package com.uh2.ateliers.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.UserDto;
import com.uh2.ateliers.service.UserService;

@Controller
public class UH2Controller {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/")
	public String root() {
		return "index";
	}
	
	@GetMapping("/login")
	public String login(Model model)  {
		return "login";
	}
	
	@GetMapping("/register")
	public String register(Model model)  {
		UserDto newUser = new UserDto();
		model.addAttribute("new_user", newUser);
		return "register";
	}
	
	@PostMapping("/register")
	public String register_new(@ModelAttribute("new_user") @Valid UserDto userDto, BindingResult result)  {
		
		User user = userService.findByUsername(userDto.getUsername());
		if(user != null) {
			result.rejectValue("username", null, "Cet utilisateur existe déjà sur la base");
		}
		
		if(result.hasErrors()) {
			return "register";
		}
		
		userService.save(userDto);
		return "redirect:/register?success";
		
	}
	
	@GetMapping("/autrepage")
	public String autrepage() {
		return "test/autrepage";
	}

}
