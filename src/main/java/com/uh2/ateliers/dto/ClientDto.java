package com.uh2.ateliers.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.uh2.ateliers.domain.Facture;

public class ClientDto {

	private Long id;
	
	private String nomClient;
	
	private String telephone;
	
	private String email;
	
	private String adresse;
	
	private List<Facture> factures;
	
	public ClientDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public ClientDto(Long id, String nomClient, String telephone, String email, String adresse,
			List<Facture> factures) {
		super();
		this.id = id;
		this.nomClient = nomClient;
		this.telephone = telephone;
		this.email = email;
		this.adresse = adresse;
		this.factures = factures;
	}

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Facture> getFactures() {
		return factures;
	}

	public void setFactures(List<Facture> factures) {
		this.factures = factures;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	
	

}

