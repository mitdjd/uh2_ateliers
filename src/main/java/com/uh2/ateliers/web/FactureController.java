package com.uh2.ateliers.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.uh2.ateliers.domain.Facture;
import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.FactureDto;
import com.uh2.ateliers.dto.UserDto;
import com.uh2.ateliers.service.FactureService;
import com.uh2.ateliers.service.UserService;

@Controller
public class FactureController {
	
	@Autowired
	private FactureService factureService;
	
	@GetMapping("/facture")
	public String facture(Model model)  {
		FactureDto newFacture = new FactureDto();
		model.addAttribute("new_facture", newFacture);
		model.addAttribute("listFactures",factureService.findAll());
		return "facture";
	}
	
	@PostMapping("/facture/add")
	public String facture_new(Model model,@ModelAttribute("new_facture") @Valid FactureDto factureDto, BindingResult result)  {
		
		Facture facture = factureService.findByReference(factureDto.getReference());
		if(facture != null) {
			result.rejectValue("nomFacture", null, "Cette facture existe déjà sur la base");
			model.addAttribute("listFactures",factureService.findAll());
		}
		
		if(result.hasErrors()) {
			return "facture";
		}
		
		factureService.save(factureDto);
		return "redirect:/facture/?success";
		
	}
}
